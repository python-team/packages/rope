pytoolconfig[global]>=1.2.2

[dev]
pytest>=7.0.1
pytest-cov>=4.1.0
pytest-timeout>=2.1.0
build>=0.7.0
pre-commit>=2.20.0

[doc]
pytoolconfig[doc]
sphinx>=4.5.0
sphinx-autodoc-typehints>=1.18.1
sphinx-rtd-theme>=1.0.0

[release]
toml>=0.10.2
twine>=4.0.2
pip-tools>=6.12.1
